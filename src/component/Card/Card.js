import styles from "./Card.module.css";
import { useState } from "react";

const Card = ({id, avatar_url, login,url,users,html_url,subscriptions_url,followers_url,organizations_url}) => {
    const[isActive,setIsActive]=useState(false);
    const ShowDetails=()=>{
       return (console.log("hiii"));
    }
return(
    <div className={styles.cardcontent}>
        <div className={styles.person}>
    <div>
    <img className={styles.img} src={avatar_url}></img>
    </div>
    <div>
    <h5>{id}</h5>
    <h5>{login}</h5>
    <h5>{url}</h5>
    </div>
    <div className={styles.btn}>
    <button className={styles.details} onClick={()=>
        setIsActive(!isActive)
    }><span>{isActive?"collpase":"Details"}</span></button>
    </div>
   </div>
   <div>
   {isActive&&<div className={styles.btndata}>
        {/* <p>{login}</p> */}
        <div className={styles.btndiv}><button className="urlbtn"><a  href={html_url} target="_blank" >HTMLUrl</a> </button>{html_url}</div>
        <div className={styles.btndiv}> <button><a  href={subscriptions_url} target="_blank" >Subscription</a></button>{subscriptions_url}</div>
        <div className={styles.btndiv}><button><a  href={followers_url} target="_blank" >Followers</a></button>{followers_url}</div>
        <div className={styles.btndiv}><button><a  href={organizations_url} target="_blank" >Organizations</a></button>{organizations_url}</div>
        </div>}
   </div>
  
  
    </div>
)
}
export default Card;



// <input type="text" placeholder="search here" name="searchitem" onchange={ReadValue}/>
//  users.map((data) => <Card  {...data}/>)
// <p>{serchItem}</p>  