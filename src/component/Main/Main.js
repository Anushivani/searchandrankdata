
import Card from "../Card/Card";
import { useState, useEffect } from "react";
import Header from "../Header/Header";
import React from "react";
import styles from "./Main.module.css";
import Pagination from "./Pagination";
import axios from 'axios';

const Main = () => {
  const [users, setUsers] = useState([]);
  const [serchItem, setSearchItem] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [postsPerpage] = useState(10);
  

  const ReadValue = (e) => {
    setSearchItem(e.target.value.substr(0, 10));
  };

  function compareAsc(a, b) {
    if (a.login < b.login) {
      return -1;
    }
    if (a.login > b.login) {
      return 1;
    }
    return 0;
  }

  function compareDsc(a, b) {
    if (a.login < b.login) {
      return 1;
    }
    if (a.login > b.login) {
      return -1;
    }
    return 0;
  }


  function compareRankup(a, b) {
    if (a.id < b.id) {
      return -1;
    }
    if (a.id > b.id) {
      return 1;
    }
    return 0;
  }

  function compareRankdowm(a, b) {
    if (a.id < b.id) {
      return 1;
    }
    if (a.id > b.id) {
      return -1;
    }
    return 0;
  }

  const ChooseOne = (e) => {
    if (e.target.value === "atoz") {
      users.sort(compareAsc);
    } else if (e.target.value === "ztoa") {
      users.sort(compareDsc);
    }else if(e.target.value==="increasing"){
        users.sort(compareRankup);
    }else if(e.target.value==="decreasing")
    {
        users.sort(compareRankdowm)
    }
    
    setUsers([...users]);
  };

  const fetchData = async (text) => {

    const res = await  axios.get(`https://api.github.com/search/users?q=${text}&client_id=${
      process.env.REACT_APP_GITHUB_CLIENT_ID}&client_secret=${ process.env.REACT_APP_GITHUB_CLIENT_SECRET}`)
      // .then((res) =>
      // res.json()
    // );
    console.log(res.data.items,"data");
    setUsers(res.data.items); 
   
      
  };
  // console.log(res.data,"data");
  console.log("users",users);
  const totalposts=users.length;
  console.log("total posts",totalposts);

  // useEffect(() => {
  // fetchData();  
  // // searchuser();
  // }, []);
  // const indexOfLastPost = currentPage * postsPerpage;
  // const indexOfFirstPost = indexOfLastPost - postsPerpage;
  // const currentPosts = users.slice(indexOfFirstPost, indexOfLastPost);
  // console.log(currentPosts,"currentPosts");
  // const paginate = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <div className={styles.main}>
      
      <Header ReadValue={ReadValue} value={serchItem} ChooseOne={ChooseOne} fetchData={fetchData}/>
      <div className={styles.totalposts}>total Posts:{totalposts}</div>
     
      {/* <Card users={users}/> */}
      {/* {currentPosts */}
      {
      users
        .filter((data) => {
          return serchItem
            ? data.login.toLowerCase().includes(serchItem)
            : true;
        })
        .map((data, i) => {
          return <Card key={i} {...data} />;
        })}
         {/* <Pagination users={users} paginate={paginate}   postsPerPage={postsPerpage} totalPosts={users.length} /> */}
    </div>
  );
};
export default Main;


// import Card from "../Card/Card";
// import {useState, useEffect} from "react";
// import Header from "../Header/Header";
// ///import styles from "./Main.module.css";
// import React from "react";

// const Main = () => {
//     const [users, setUsers] = useState([]);
//     const[serchItem,setSearchItem]=useState();
//     const ReadValue=(e)=>{
//         setSearchItem(e.target.value.substr(0,10));
//     }
//     function compare( a, b ) {
//         if ( a.login < b.login ){
//           return -1;
//         }
//         if ( a.login > b.login ){
//           return 1;
//         }
//         return 0;
//       }
//     const ChooseOne=(e)=>{
//       if(e.target.value==="atoz")
//       {
//           users.sort( compare );
//           setUsers(users);
//           console.log("temp",users);
//       }
      
//        console.log("sortby");
//     }

//         const FetchData = async () => {
//         const res = await fetch("https://api.github.com/users");
//         setUsers(await res.json());
//        }
//        console.log(users);
//        useEffect(() => {
//             FetchData();
//        }, []);
//        let filterUsersData=users.filter((data)=>{
   
//            return (data.login.toLowerCase().indexOf(serchItem)!==-1);
//        })

//        return(
//            <div>
//                 <Header ReadValue={ReadValue} value={serchItem} ChooseOne={ChooseOne}/>
//                 {
//                     filterUsersData.map((data,i)=>{
//                         return  <Card key={i} {...data}/>
//                     }) 
//                 }
    

//            </div>
//        )

// }
// export default Main;