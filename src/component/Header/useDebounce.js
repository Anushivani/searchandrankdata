import {useCallback} from "react";
import { debounce } from "lodash";
export const useDebounce = (callback,delay) => {
    const debouncecallback = useCallback(debounce(callback,delay),[delay]);
    return debouncecallback;
}
