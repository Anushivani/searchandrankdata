import { useEffect, useState } from 'react';
import styles from './Header.module.css';
import {useDebounce} from "./useDebounce";

const Header = ({ChooseOne,fetchData}) =>  {
    const [text,setText]=useState("");

    const debounceFun = useDebounce((val) => setText(val),700);
 const handleChange = (e) => {
    debounceFun(e.target.value);
  }

   

   console.log("text",text);
    useEffect(()=>{
        if(text)
        fetchData(text);
    },[text])
    
    
       
            
    
    return(
<div className={styles.nav}>

<span>
<select
       className={styles.filter}  onChange={ChooseOne}
       label="Sort By Name">
        <option  value="All">Sort By Name</option>
        <option value="atoz">Name (A-Z)</option>
        <option value="ztoa">Name (Z-A)</option>
        <option value="increasing">Rank Up</option>
        <option value="decreasing">Rank Down</option>
</select>
</span>
<span>
{/* <input className={styles.search} type="search" placeholder="Search" onchange={serchElement}></input> */}
<input className={styles.search} type="text" placeholder="Search here"   onChange={handleChange}></input>
    {/* console.log({element}); */}
</span>


</div>)
}

export default Header;